import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import java.io.*;

import java.util.ArrayList;

import javax.imageio.ImageIO;

/*
 * important notes
 * 
 * -only parses images that are very square
 * -there is an extra black row and column on the top and left
 * -image is backwards and rotated 
 */



public class Art {
	BufferedImage image;
	int boxsize;
	
	public Art(String input, int value) {
		try {
			image = ImageIO.read(new File(input));
			boxsize = value;
			greyscale();
			int[][] intcolors = parseImage();
			output(intcolors);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void greyscale() {
		BufferedImage greyimg = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);  
		Graphics g = greyimg.getGraphics();
		g.drawImage(image, 0, 0, null);
		image = greyimg;
	}
	
	private int[][] parseImage() {
		int rows = image.getHeight()/boxsize;
		int cols = image.getWidth()/boxsize;
		int[][] intcolors = new int[rows][cols];
		for (int r = 1; r < rows; r++) {
			for (int c = 1; c < cols; c++) {
				intcolors[r][c] = findBoxColor(r,c);
			}
		}
		return intcolors;
	}
	
	private int findBoxColor(int row, int col) {
		ArrayList<Color> boxcolors = new ArrayList<Color>();
		for (int x = row*boxsize-boxsize; x < row*boxsize; x++) {
			for (int y = col*boxsize-boxsize; y < col*boxsize; y++) {
				Color c = new Color(image.getRGB(x,y));
				boxcolors.add(c);
			}
		}
		int sum = 0;
		for (int x = 0; x < boxcolors.size(); x++) {
			sum += boxcolors.get(x).getRed();
		}
		return sum/boxcolors.size();
	}
	
	private void output(int[][] intcolors) {
		for (int row = 1; row < intcolors.length; row++) {
			for (int col = 1; col < intcolors[0].length; col++) {
				if (intcolors[row][col] >= 225) {
					System.out.print(". ");
				}
				else if (intcolors[row][col] >= 200) {
					System.out.print("* ");
				}
				else if (intcolors[row][col] >= 175) {
					System.out.print("- ");
				}
				else if (intcolors[row][col] >= 150) {
					System.out.print("! ");
				}
				else if (intcolors[row][col] >= 125) {
					System.out.print("O ");
				}
				else if (intcolors[row][col] >= 100) {
					System.out.print("8 ");
				}
				else if (intcolors[row][col] >= 50) {
					System.out.print("N ");
				}
				else if (intcolors[row][col] >= 25) {
					System.out.print("# ");
				}
				else {
					System.out.print("@ ");
				}
			}
			System.out.println();	
		}
	}
}