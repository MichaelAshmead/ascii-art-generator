import java.io.FileOutputStream;

import com.pdfjet.*;


public class PDFcreator {
	public void createPDF() throws Exception {
    	FileOutputStream fos = new FileOutputStream("output.pdf");
        
    	PDF pdf = new PDF(fos);
    	Page page = new Page(pdf, Letter.PORTRAIT);
    	
    	Font f1 = new Font(pdf, CoreFont.COURIER);
    	f1.setSize(8);

        TextColumn column = new TextColumn(f1);
        
        Paragraph p1 = new Paragraph();
        p1.setAlignment(Align.LEFT);
        TextLine text = new TextLine(f1);
        text.setText("The Swiss Confederation was founded in 1291 as a defensive alliance among three cantons. In succeeding years, other localities joined the original three. The Swiss Confederation secured its independence from the Holy Roman Empire in 1499. Switzerland's sovereignty and neutrality have long been honored by the major European powers, and the country was not involved in either of the two World Wars. The political and economic integration of Europe over the past half century, as well as Switzerland's role in many UN and international organizations, has strengthened Switzerland's ties with its neighbors. However, the country did not officially become a UN member until 2002.");
        text.setFont(f1);
        p1.add(text);

        column.addParagraph(p1);

        column.setPosition(40, 40);

        column.setSize(520, 100);

        column.drawOn(page);

        pdf.flush();
        fos.close();
    }
}
